const params = new URLSearchParams(window.location.search)
const sequenceNum = params.get('sequence')

document.getElementById('sequenceDisplay').innerText = sequenceNum
document.getElementsByTagName('button')[0].addEventListener('click', () => {
    const nextSequenceNum = Number(sequenceNum) + 1
    window.location.assign(`callback.html?sequence=${nextSequenceNum}`)
})
