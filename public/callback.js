const params = new URLSearchParams(window.location.search)
const sequenceNum = params.get('sequence')

const bc = new BroadcastChannel('callback')
bc.postMessage(sequenceNum)
bc.close()
