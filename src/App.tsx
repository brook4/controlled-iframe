import React, { useRef, useEffect } from 'react'
import './App.css'

const getExternalUrl: (sequenceNum: string) => string = sequenceNum => `external.html?sequence=${sequenceNum}`

function App() {
  const frame = useRef<HTMLIFrameElement | null>(null)

  useEffect(() => {
    frame.current!.src = getExternalUrl(String(0))
  }, [])

  useEffect(() => {
    const bc = new BroadcastChannel('callback')
    bc.onmessage = (message) => {
      const url = getExternalUrl(message.data as string)
      frame.current!.src = url
    }
    return () => bc.close()
  })

  return (
    <div>
      <h1>Controlled Inline Frame Demo</h1>
      <iframe ref={frame} title="external site"></iframe>
    </div>
  );
}

export default App;
